package com.test.airlinebooking.repository;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.FlightStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {
	@Override
	List<Flight> findAll();
	Optional<Flight> findById(Long id);
	List<Flight> findAllByUserIdOrderById(Long id);
	List<Flight> findAllByStatusIsIn(List<FlightStatus> flightStatuses);
	List<Flight> findAllByStatusIsNotIn(List<FlightStatus> flightStatuses);
}
