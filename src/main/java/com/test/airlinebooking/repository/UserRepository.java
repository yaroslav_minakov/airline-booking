package com.test.airlinebooking.repository;

import com.test.airlinebooking.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Override
	List<User> findAll();
	User findByEmail(final String email);
	Optional<User> findById(final Long id);
}
