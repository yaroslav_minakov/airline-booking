package com.test.airlinebooking.repository;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

	Optional<Ticket> findById(Long id);
	List<Ticket> findAllByFlight(Flight flight);
	List<Ticket> findAllByFlightIsIn(List<Flight> flights);
	List<Ticket> findAllByUserIdOrderByDateOfPurchase(Long id);
}
