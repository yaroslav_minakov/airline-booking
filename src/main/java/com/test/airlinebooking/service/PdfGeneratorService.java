package com.test.airlinebooking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.util.Map;
import java.util.UUID;

@Component
public class PdfGeneratorService {

	@Autowired
	private TemplateEngine templateEngine;

	public InputStream createPdf(String templateName, Map map) throws Exception {
		Assert.notNull(templateName, "The templateName can not be null");
		Context ctx = new Context();
		if (map != null) {
			for (Object o : map.entrySet()) {
				Map.Entry pair = (Map.Entry) o;
				ctx.setVariable(pair.getKey().toString(), pair.getValue());
			}
		}

		String processedHtml = templateEngine.process(templateName, ctx);
		FileOutputStream os = null;
		String fileName = UUID.randomUUID().toString();
		final File outputFile = File.createTempFile(fileName, ".pdf");
		try {
			os = new FileOutputStream(outputFile);

			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocumentFromString(processedHtml);
			renderer.layout();
			renderer.createPDF(os, false);
			renderer.finishPDF();
		}
		finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) { /*ignore*/ }
			}
		}
		return new FileInputStream(outputFile);
	}
}
