package com.test.airlinebooking.service;

import com.test.airlinebooking.controller.dto.UserRegistrationDTO;
import com.test.airlinebooking.entity.Role;
import com.test.airlinebooking.entity.User;
import com.test.airlinebooking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository repository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public User findByEmail(String email) {
		return repository.findByEmail(email);
	}

	@Transactional
	public void save(UserRegistrationDTO registration) {
		User user = new User();
		user.setUsername(registration.getUsername());
		user.setEmail(registration.getEmail());
		user.setPassword(passwordEncoder.encode(registration.getPassword()));
		user.setBalance(BigDecimal.ZERO);
		user.setRole(registration.getRole());
		user.setFlights(registration.getFlights());
		user.setTickets(registration.getTickets());
		repository.save(user);
	}

	@Transactional
	public void update(User user) {
		repository.saveAndFlush(user);
	}

	public User findById(Long id) {
		return repository.findById(id).orElseThrow(() -> new UsernameNotFoundException(format("User with id %s not found", id)));
	}

	@Override
	public UserDetails loadUserByUsername(final String name) throws UsernameNotFoundException {
		User user = findByEmail(name);
		if (Objects.isNull(user)) {
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getEmail(),
				user.getPassword(),
				buildUserAuthority(user.getRole()));
	}

	private Collection<? extends GrantedAuthority> buildUserAuthority(Role role) {
		return Collections.singletonList(new SimpleGrantedAuthority(role.toString()));
	}

	public User save(User company) {
		return repository.save(company);
	}
}
