package com.test.airlinebooking.service;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.FlightStatus;
import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.entity.TicketType;
import com.test.airlinebooking.repository.FlightRepository;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FlightService {

	@Autowired
	private FlightRepository flightRepository;

	@Transactional
	public Flight save(Flight flight) {
		return flightRepository.save(flight);
	}

	@Transactional
	public void update(Flight flight) {
		flightRepository.saveAndFlush(flight);
	}

	public Flight findById(Long id) {
		try {
			return flightRepository.findById(id).orElseThrow(Exception::new);
		} catch (Exception e) {}
		return null;
	}

	public double ticketPriceByType(TicketType type, Long flightId) {
		Flight flight = findById(flightId);
		switch (type) {
			case ECONOMY:
				return flight.getEconomyPrice();
			case COMFORT:
				return flight.getComfortPrice();
			case BUSINESS:
				return flight.getBusinessPrice();
			default:
				return 0.0;
		}
	}

	public List<Flight> findFlightsByStatusesIsNotIn(List<FlightStatus> flightStatuses) {
		return flightRepository.findAllByStatusIsNotIn(flightStatuses);
	}

	public List<Flight> findFlightsByUserId(Long id) {
		return flightRepository.findAllByUserIdOrderById(id);
	}

	public int isAvailableTickets(Ticket ticket) {
		Flight flight = ticket.getFlight();
		switch (ticket.getType()) {
			case ECONOMY:
				return flight.getEconomyClass();
			case COMFORT:
				return flight.getComfortClass();
			case BUSINESS:
				return flight.getBusinessClass();
			default:
				return 0;
		}
	}

	public List<Flight> findAvailableFlights() {
		return flightRepository.findAllByStatusIsIn(Arrays.asList(FlightStatus.ON_TIME, FlightStatus.DELAYED));
	}

	public Map<Long, String> findFlightTimesByFlights() {
		return findAvailableFlights().stream()
				.collect(Collectors.toMap(Flight::getId,
						flight -> DurationFormatUtils.formatDuration(
								Duration.between(flight.getDepartureTime(), flight.getArrivalTime()).toMillis(),
								"HH:mm",
								true)));
	}

	public boolean isDelayed(Flight flight) {
		return flight.getDepartureTime().isAfter(LocalDateTime.now()) &&
				flight.getDepartureTime().isBefore(LocalDateTime.now().plusDays(1));
	}
}
