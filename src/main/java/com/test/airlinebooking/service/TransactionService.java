package com.test.airlinebooking.service;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class TransactionService {

	@Autowired
	private UserService userService;

	@Autowired
	private FlightService flightService;

	public void purchasing(Ticket ticket) {
		//entity
		Flight flight = ticket.getFlight();
		User company = flight.getUser();
		User customer = ticket.getUser();
		//balance
		BigDecimal companyBalance = company.getBalance();
		BigDecimal customerBalance = customer.getBalance();
		BigDecimal totalPrice = BigDecimal.valueOf(ticket.getTotalPrice());
		//calculating
		company.setBalance(companyBalance.add(totalPrice));
		customer.setBalance(customerBalance.subtract(totalPrice));
		//update
		userService.update(company);
		userService.update(customer);
		flightService.update(reserving(ticket, flight));
	}

	private Flight reserving(Ticket ticket, Flight flight) {
		switch (ticket.getType()) {
			case ECONOMY:
				flight.setEconomyClass(flight.getEconomyClass() - ticket.getCount());
				break;
			case COMFORT:
				flight.setComfortClass(flight.getComfortClass() - ticket.getCount());
				break;
			case BUSINESS:
				flight.setBusinessClass(flight.getBusinessClass() - ticket.getCount());
				break;
		}
		return flight;
	}
}
