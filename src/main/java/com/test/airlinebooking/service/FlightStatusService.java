package com.test.airlinebooking.service;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.FlightStatus;
import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

@Service
public class FlightStatusService {

	@Autowired
	private FlightService flightService;

	@Autowired
	private TicketService ticketService;

	@Autowired
	private UserService userService;

	private ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");

	public void changeToDelay(Flight flight, Duration difference) {
		if (flightService.isDelayed(flight)) {
			User company = flight.getUser();
			List<Ticket> tickets = ticketService.findTicketsByFlight(flight);
			Integer percent = Integer.valueOf(resourceBundle.getString("cashback.percent"));
			if (!tickets.isEmpty()) {
				for (Ticket ticket : tickets) {
					User customer = ticket.getUser();
					BigDecimal price = BigDecimal.valueOf((ticket.getTotalPrice()/100) * percent);
					customer.setBalance(customer.getBalance().add(price));
					company.setBalance(company.getBalance().subtract(price));
					userService.update(customer);
				}
				userService.update(company);
			}
			flight.setArrivalTime(flight.getDepartureTime().plus(difference));
			flight.setStatus(FlightStatus.DELAYED);
			flightService.update(flight);
		}
	}

	@Scheduled(fixedRate = 60000)
	public void flightStatusChanger() {
		LocalDateTime now = LocalDateTime.now();
		List<FlightStatus> flightStatuses = Collections.singletonList(FlightStatus.DONE);
		List<Flight> flights = flightService.findFlightsByStatusesIsNotIn(flightStatuses);
		flights.stream()
				.filter(flight -> flight.getDepartureTime().isAfter(now) &&
						flight.getDepartureTime().isBefore(now.plusHours(1)))
				.forEach(flight -> flight.setStatus(FlightStatus.BOARDING));
		flights.stream()
				.filter(flight -> flight.getDepartureTime().isBefore(now) &&
						flight.getArrivalTime().isAfter(now))
				.forEach(flight -> flight.setStatus(FlightStatus.ON_WAY));
		flights.stream()
				.filter(flight -> flight.getArrivalTime().isBefore(now))
				.forEach(flight -> flight.setStatus(FlightStatus.DONE));
		flights.forEach(flight -> flightService.update(flight));
	}
}
