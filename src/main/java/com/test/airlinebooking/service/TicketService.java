package com.test.airlinebooking.service;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TicketService {

	@Autowired
	private TicketRepository ticketRepository;

	@Transactional
	public void save(Ticket ticket) {
		ticket.setDateOfPurchase(LocalDateTime.now());
		ticketRepository.save(ticket);
	}

	public Ticket findById(Long id) throws Exception {
		return ticketRepository.findById(id).orElseThrow(Exception::new);
	}

	public List<Ticket> findTicketsByUserId(Long id) {
		return ticketRepository.findAllByUserIdOrderByDateOfPurchase(id);
	}

	public List<Ticket> findTicketsByFlights(List<Flight> flights) {
		return ticketRepository.findAllByFlightIsIn(flights);
	}

	public List<Ticket> findTicketsByFlight(Flight flight) {
		return ticketRepository.findAllByFlight(flight);
	}

	public Map<Long, Map<String, Integer>> sortTickets(List<Ticket> tickets) {
		for (int i = 0; i < tickets.size(); i++) {
			for (int j = i + 1; j < tickets.size(); j++) {
				if (tickets.get(i).getFlight().equals(tickets.get(j).getFlight()) &&
						tickets.get(i).getType().equals(tickets.get(j).getType())) {
					tickets.get(i).setCount(tickets.get(i).getCount() + tickets.get(j).getCount());
					tickets.remove(j);
				}
			}
		}
		return tickets.stream()
				.collect(Collectors.groupingBy(ticket -> ticket.getFlight().getId(),
						Collectors.toMap(ticket -> ticket.getType().toString(), Ticket::getCount)));
	}
}
