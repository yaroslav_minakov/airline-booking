package com.test.airlinebooking.entity;

public enum FlightStatus {
	ON_TIME, DELAYED, BOARDING, ON_WAY, DONE
}
