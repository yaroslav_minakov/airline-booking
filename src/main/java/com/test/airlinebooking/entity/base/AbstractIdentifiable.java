package com.test.airlinebooking.entity.base;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractIdentifiable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;


}
