package com.test.airlinebooking.entity;

import com.test.airlinebooking.entity.base.AbstractIdentifiable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "tickets")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ticket extends AbstractIdentifiable {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "flight_id", nullable = false)
	private Flight flight;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	@Column(name = "ticket_type")
	@Enumerated(EnumType.STRING)
	private TicketType type;

	private int count;

	@Column(name = "total_price")
	private double totalPrice;

	@Column(name = "date_of_purchase")
	private LocalDateTime dateOfPurchase;

	public Ticket(Flight flight, User user, TicketType type) {
		this.flight = flight;
		this.user = user;
		this.type = type;
	}
}
