package com.test.airlinebooking.entity;

import com.test.airlinebooking.entity.base.AbstractIdentifiable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(name = "flights")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Flight extends AbstractIdentifiable {

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;

	@NotEmpty(message = "Enter departure place")
	@Column(name = "place_of_departure")
	private String placeOfDeparture;

	@NotEmpty(message = "Enter arrival place")
	@Column(name = "place_of_arrival")
	private String placeOfArrival;

	@NotNull(message = "Enter departure date")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME )
	@Column(name = "departure_time")
	private LocalDateTime departureTime;

	@NotNull(message = "Enter arrival date")
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	@Column(name = "arrival_time")
	private LocalDateTime arrivalTime;

	@Enumerated(EnumType.STRING)
	private FlightStatus status;

	@Column(name = "economy", nullable = false)
	private int economyClass;

	@Column(name = "comfort", nullable = false)
	private int comfortClass;

	@Column(name = "business", nullable = false)
	private int businessClass;

	@Column(name = "economy_price", nullable = false)
	private double economyPrice;

	@Column(name = "comfort_price", nullable = false)
	private double comfortPrice;

	@Column(name = "business_price", nullable = false)
	private double businessPrice;
}
