package com.test.airlinebooking.entity;

public enum TicketType {
	ECONOMY, COMFORT, BUSINESS
}
