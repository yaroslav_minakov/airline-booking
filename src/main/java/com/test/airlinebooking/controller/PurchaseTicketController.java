package com.test.airlinebooking.controller;

import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.entity.TicketType;
import com.test.airlinebooking.service.FlightService;
import com.test.airlinebooking.service.TicketService;
import com.test.airlinebooking.service.TransactionService;
import com.test.airlinebooking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.security.Principal;

@Controller
public class PurchaseTicketController {

	@Autowired
	private UserService userService;

	@Autowired
	private FlightService flightService;

	@Autowired
	private TicketService ticketService;

	@Autowired
	private TransactionService transactionService;

	@GetMapping("/ticket-purchase/")
	public String ticketPurchase(@RequestParam("company_id") Long companyId,
								 @RequestParam("flight_id") Long flightId,
								 @RequestParam("ticket_type") String ticketType,
								 Principal principal,
								 Model model) {
		Ticket ticket = new Ticket(flightService.findById(flightId),
				userService.findByEmail(principal.getName()),
				TicketType.valueOf(ticketType));
		model.addAttribute("companyId", companyId);
		model.addAttribute("ticket", ticket);
		model.addAttribute("price",
				flightService.ticketPriceByType(TicketType.valueOf(ticketType), flightId));
		model.addAttribute("availableTickets", flightService.isAvailableTickets(ticket));
		return "purchase-ticket-form";
	}

	@PostMapping("/ticket-purchase-processing")
	public String ticketPurchaseProcessing(@ModelAttribute Ticket ticket,
										   @RequestParam Long companyId,
										   @RequestParam double priceByTicket) {
		if (flightService.isAvailableTickets(ticket) > 0) {
			ticket.setTotalPrice(ticket.getCount() * priceByTicket);
			ticket.getFlight().setUser(userService.findById(companyId));
			if (ticket.getUser().getBalance().compareTo(BigDecimal.valueOf(ticket.getTotalPrice())) >= 0) {
				transactionService.purchasing(ticket);
				ticketService.save(ticket);
				return "redirect:/flights";
			}
			return "redirect:/flights?purchaseError";
		}
		return "redirect:/flights?purchaseError";
	}
}
