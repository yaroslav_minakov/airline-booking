package com.test.airlinebooking.controller.dto;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.Role;
import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.validation.registration.EmailValidator;
import com.test.airlinebooking.validation.registration.FieldMatch;
import com.test.airlinebooking.validation.registration.ValidPassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@FieldMatch.List({
		@FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
		@FieldMatch(first = "email", second = "confirmEmail", message = "The email fields must match")
})
public class UserRegistrationDTO {

	@Size(min = 5, max = 30, message = "The size should be between 5 and 30")
	private String username;

	@Pattern(regexp = EmailValidator.REGEX, message = "Wrong email format")
	private String email;

	@Pattern(regexp = EmailValidator.REGEX, message = "Wrong email format")
	private String confirmEmail;

	@ValidPassword
	private String password;

	@ValidPassword
	private String confirmPassword;

	@NotNull(message = "Select role")
	private Role role;

	@AssertTrue(message = "You must accept registration terms")
	private Boolean terms;

	private List<Ticket> tickets = new ArrayList<>();
	private List<Flight> flights = new ArrayList<>();
}
