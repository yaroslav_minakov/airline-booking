package com.test.airlinebooking.controller;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.FlightStatus;
import com.test.airlinebooking.service.FlightService;
import com.test.airlinebooking.service.FlightStatusService;
import com.test.airlinebooking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.time.Duration;
import java.time.LocalDateTime;

@Controller
public class FlightController {

	private static final String DEPARTURE_CHANGE_FORM = "departure-change-form";
	private static final String NEW_FLIGHT_FORM = "new-flight";

	@Autowired
	private FlightService flightService;

	@Autowired
	private  UserService userService;

	@Autowired
	private FlightStatusService flightStatusService;


	@GetMapping("/flights")
	public String getFlightList(Model model,
								@RequestParam(value = "success", required = false) String success,
								@RequestParam(value = "purchaseError", required = false) String purchaseError) {
		model.addAttribute("purchaseError", purchaseError != null);
		model.addAttribute("success", success != null);
		model.addAttribute("flights", flightService.findAvailableFlights());
		model.addAttribute("flightTimes", flightService.findFlightTimesByFlights());
		return "flights";
	}

	@GetMapping("/new-flight")
	public String newFlightForm(Model model, Principal principal) {
		Flight flight = new Flight();
		flight.setStatus(FlightStatus.ON_TIME);
		flight.setUser(userService.findByEmail(principal.getName()));
		model.addAttribute("flight", flight);
		return NEW_FLIGHT_FORM;
	}

	@PostMapping("/new-flight")
	public String newFlightForm(@ModelAttribute("flight") @Valid Flight flight, BindingResult result) {
		if (result.hasErrors()) {
			return NEW_FLIGHT_FORM;
		}
		if (flight.getDepartureTime().isBefore(LocalDateTime.now())) {
			result.rejectValue("departureTime", null, "Departure can't be in past");
			return NEW_FLIGHT_FORM;
		}
		if (flight.getArrivalTime().isBefore(flight.getDepartureTime())) {
			result.rejectValue("arrivalTime", null, "Departure can't be after arrival");
			return NEW_FLIGHT_FORM;
		}
		if ((flight.getEconomyClass() + flight.getComfortClass() + flight.getBusinessClass()) == 0) {
			result.rejectValue("economyClass", null, "Create at least 1 seat");
			result.rejectValue("comfortClass", null, "Create at least 1 seat");
			result.rejectValue("businessClass", null, "Create at least 1 seat");
			return NEW_FLIGHT_FORM;
		}
		flightService.save(flight);
		return "redirect:/flights?success";
	}

	@GetMapping("/departure-date-change-form")
	public String departureDateChangeForm(@RequestParam Long flightId, Model model) {
		Flight flight = flightService.findById(flightId);
		String difference = Duration.between(flight.getDepartureTime(), flight.getArrivalTime()).toString();
		if (!flight.getDepartureTime().isAfter(LocalDateTime.now().plusHours(1).plusMinutes(30))) {
			return "redirect:/company-profile";
		}
		model.addAttribute("difference", difference);
		model.addAttribute("delayedStatus", FlightStatus.DELAYED);
		model.addAttribute("isDelayed", flightService.isDelayed(flight));
		model.addAttribute("flight", flight);
		return DEPARTURE_CHANGE_FORM;
	}

	@PostMapping("/departure-date-change-processing/")
	public String departureChange(@ModelAttribute("flight") @Valid Flight flight,
								  @RequestParam("difference") String duration,
								  BindingResult result) {
		if (flight.getDepartureTime().isBefore(LocalDateTime.now())) {
			result.rejectValue("departureTime", null,"Departure can't be in past");
			return DEPARTURE_CHANGE_FORM;
		}
		if (flight.getArrivalTime().isBefore(flight.getDepartureTime())) {
			result.rejectValue("departureTime", null, "Departure can't be after arrival");
			return DEPARTURE_CHANGE_FORM;
		}
		if (result.hasErrors()) {
			return DEPARTURE_CHANGE_FORM;
		}
		flightStatusService.changeToDelay(flight, Duration.parse(duration));
		return "redirect:/company-profile";
	}
}
