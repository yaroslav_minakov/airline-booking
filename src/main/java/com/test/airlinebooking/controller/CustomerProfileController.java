package com.test.airlinebooking.controller;

import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.entity.User;
import com.test.airlinebooking.service.TicketService;
import com.test.airlinebooking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

@Controller
public class CustomerProfileController {

	@Autowired
	private UserService userService;

	@Autowired
	private TicketService ticketService;

	@GetMapping("/customer-profile")
	public String userProfile(Principal principal, Model model) {
		User user = userService.findByEmail(principal.getName());
		List<Ticket> tickets = ticketService.findTicketsByUserId(user.getId());
		Collections.reverse(tickets);
		model.addAttribute("currentUser", user);
		model.addAttribute("tickets", tickets);
		return "customer-profile";
	}

	@PostMapping("/fill-up-balance")
	public String fillUpBalance(Principal principal) {
		User user = userService.findByEmail(principal.getName());
		user.setBalance(user.getBalance().add(BigDecimal.valueOf(1000)));
		userService.update(user);
		return "redirect:/customer-profile";
	}
}
