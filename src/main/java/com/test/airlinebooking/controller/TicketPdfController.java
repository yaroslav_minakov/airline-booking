package com.test.airlinebooking.controller;

import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.service.PdfGeneratorService;
import com.test.airlinebooking.service.TicketService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TicketPdfController {

	@Autowired
	private PdfGeneratorService pdfGeneratorService;

	@Autowired
	private TicketService ticketService;

	@GetMapping(value = "/ticket-pdf")
	public void ticketPdf(@RequestParam Long id, HttpServletResponse response) throws Exception {
		Map<String, Object> data = new HashMap<>();
		Ticket ticket = ticketService.findById(id);
		data.put("ticketNumber", ticket.getId());
		data.put("purchaseTime", ticket.getDateOfPurchase());
		data.put("name", ticket.getUser().getUsername());
		data.put("count", ticket.getCount());
		data.put("departureTime", ticket.getFlight().getDepartureTime());
		data.put("arrivalTime", ticket.getFlight().getArrivalTime());
		data.put("departurePlace", ticket.getFlight().getPlaceOfDeparture());
		data.put("arrivalPlace", ticket.getFlight().getPlaceOfArrival());
		data.put("class", ticket.getType().toString());
		data.put("flightId", ticket.getFlight().getId());
		response.addHeader("Content-disposition", "attachment;filename=ticket.pdf");
		response.setContentType("application/pdf");
		try(InputStream is = pdfGeneratorService.createPdf("ticket-pdf", data)) {
			IOUtils.copy(is, response.getOutputStream());
		} finally {
			response.flushBuffer();
		}
	}
}
