package com.test.airlinebooking.controller;

import com.test.airlinebooking.entity.Flight;
import com.test.airlinebooking.entity.Ticket;
import com.test.airlinebooking.entity.User;
import com.test.airlinebooking.service.FlightService;
import com.test.airlinebooking.service.TicketService;
import com.test.airlinebooking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Controller
public class CompanyProfileController {

	@Autowired
	private UserService userService;

	@Autowired
	private FlightService flightService;

	@Autowired
	private TicketService ticketService;


	@GetMapping("/company-profile")
	public String userProfile(Principal principal, Model model) {
		User user = userService.findByEmail(principal.getName());
		List<Flight> flights = flightService.findFlightsByUserId(user.getId());
		List<Ticket> tickets = ticketService.findTicketsByFlights(flights);
		Collections.reverse(flights);
		model.addAttribute("flights", flights);
		model.addAttribute("currentUser", user);
		model.addAttribute("now", LocalDateTime.now());
		model.addAttribute("sorted", ticketService.sortTickets(tickets));
		return "company-profile";
	}
}
