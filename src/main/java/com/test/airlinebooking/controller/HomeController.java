package com.test.airlinebooking.controller;

import com.test.airlinebooking.controller.dto.UserRegistrationDTO;
import com.test.airlinebooking.entity.User;
import com.test.airlinebooking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class HomeController {

	private static final String REGISTRATION_FORM = "registration";

	@Autowired
	private UserService userService;


	@GetMapping("/")
	public String home() {
		return "home";
	}

	@GetMapping(value = "/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response,
						 RedirectAttributes redirectAttributes) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		redirectAttributes.addAttribute("logout", true);
		return "redirect:/login";
	}

	@GetMapping("/login")
	public String login(Model model,
						@RequestParam(value = "error", required = false) String error,
						@RequestParam(value = "logout", required = false) Boolean logout,
						@RequestParam(value = "success", required = false) String success
	) {
		model.addAttribute("error", error != null);
		model.addAttribute("logout", logout != null);
		model.addAttribute("success", success != null);
		return "login";
	}

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("user", new UserRegistrationDTO());
		return REGISTRATION_FORM;
	}

	@PostMapping("/registration")
	public String registration(@ModelAttribute("user") @Valid UserRegistrationDTO user, BindingResult result) {
		User existing = userService.findByEmail(user.getEmail());
		if (result.hasErrors()) {
			return REGISTRATION_FORM;
		}
		if (existing != null) {
			result.rejectValue("email", null, "There is already an account registered with that email");
			return REGISTRATION_FORM;
		}
		userService.save(user);
		return "redirect:/login?success";
	}
}
