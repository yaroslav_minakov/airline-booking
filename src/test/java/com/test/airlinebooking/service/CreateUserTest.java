package com.test.airlinebooking.service;

import com.test.airlinebooking.entity.*;
import com.test.airlinebooking.repository.FlightRepository;
import com.test.airlinebooking.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CreateUserTest {

	protected User company;
	protected User customer;
	protected Flight flight;
	protected Ticket ticket;

	@Mock
	protected UserRepository userRepository;

	@Mock
	protected FlightRepository flightRepository;

	@InjectMocks
	protected UserService userService;

	@InjectMocks
	protected FlightService flightService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		ticket = new Ticket(flight, customer, null, 1, 0.0, LocalDateTime.now());
		flight = new Flight(company, "departure", "arrival", LocalDateTime.now().plusHours(1), LocalDateTime.now().plusHours(4), FlightStatus.ON_TIME, 30, 20, 10, 300, 400, 500);
		customer = new User("customer", "customer@mail.com", "password", BigDecimal.valueOf(2000), Role.CUSTOMER, Collections.emptyList(), Collections.emptyList());
		company = new User("company", "company@mail.com", "password", BigDecimal.ZERO, Role.COMPANY, Collections.emptyList(), Collections.emptyList());
	}

	@Test
	public void createTest() {
		when(userRepository.save(company)).thenReturn(company);
		User resultCompany = userService.save(company);
		assertEquals(company, resultCompany);

		when(userRepository.save(customer)).thenReturn(customer);
		User resultCustomer = userService.save(customer);
		assertEquals(customer, resultCustomer);

		when(flightRepository.save(flight)).thenReturn(flight);
		Flight resultFlight = flightService.save(flight);
		assertEquals(flight, resultFlight);
	}
}
